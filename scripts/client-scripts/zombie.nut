/* Client side */
function createDraw(text, font, x, y, r, g, b, visible)
{
    local _draw = Draw(x, y, text);
    _draw.font = font;
    _draw.setColor(r, g, b);
    _draw.visible = visible;
    return _draw;
}

function irand(max)
{
    local roll = (1.0 * rand() / RAND_MAX) * (max + 1);
    return roll.tointeger();
}

local Draws = {};
Draws.time <- createDraw("00:00", "Font_Old_10_White_Hi.TGA", 6500, 5700, 255, 255, 255, false);
Draws.zombies <- createDraw("Zombies: 0", "Font_Old_10_White_Hi.TGA", 6500, 6000, 255, 0, 0, false);
Draws.humans <- createDraw("Humans: 0", "Font_Old_10_White_Hi.TGA", 6500, 6300, 0, 255, 0, false);
Draws.round <- createDraw("Begin the game in:", "Font_Old_10_White_Hi.TGA", 6500, 5400, 255, 0, 255, false);

local GameText = {};
GameText.draw <- Draw(0, 0, "NULL");
GameText.timer <- 0;

local Game = {};
Game.started <- false;
Game.minute <- 0;
Game.second <- 59;
Game.zombies <- 0;
Game.humans <- 0;
Game.team <- -1;

function gameText(x, y, text, font, r, g, b, time)
{
    GameText.draw.setPosition(x, y);
    GameText.draw.setColor(r, g, b);
    GameText.draw.font = font;
    GameText.draw.text = text;
    GameText.draw.visible = true;
    killTimer(GameText.timer);
    GameText.timer = setTimer(function()
    {
        GameText.draw.visible = false;
    }, time, 1);
}

addEventHandler("onInit", function()
{
    Chat.print(0, 255, 255, "----====Zombie Mode 1.3 by Risen (G2O port: Sative) ====----");
    foreach(draw in Draws)
        draw.visible = true;
});

addEventHandler("onPacket", function(packet)
{
    local id = packet.readUInt8();
    switch(id)
    {
        case MessageId.GAME_TEXT:
        local x = packet.readUInt16();
        local y = packet.readUInt16();
        local r = packet.readUInt8();
        local g = packet.readUInt8();
        local b = packet.readUInt8();
        local time = packet.readUInt16();
        local font = packet.readString();
        local text = packet.readString();
        gameText(x, y, text, font, r, g, b, time.tointeger());
        break;

        case MessageId.CLIENT_UPDATE:
        Game.started = packet.readBool();
        Game.minute = packet.readUInt8();
        Game.second = packet.readUInt8();
        Game.zombies = packet.readUInt8();
        Game.humans = packet.readUInt8();

        if(Game.started == false)
            Draws.round.text = "Begin the game in:";
        else Draws.round.text = "End of round in:";

        Draws.time.text = format("%d:%02d", Game.minute, Game.second);
        Draws.zombies.text = format("Zombies: %d", Game.zombies);
        Draws.humans.text = format("Humans: %d", Game.humans);

        break;

        case MessageId.SET_ZOMBIE:
        Game.team = Teams.ZOMBIE;
        break;

        case MessageId.SET_HUMAN:
        Game.team = Teams.HUMAN;
        break;
    }
});

print("zombie.nut loaded");