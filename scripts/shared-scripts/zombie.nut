enum Teams {
    ZOMBIE,
    HUMAN
}

enum MessageId
{
    CLIENT_UPDATE,
    GAME_TEXT,
    SET_ZOMBIE,
    SET_HUMAN
}