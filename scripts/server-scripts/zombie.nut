/* Server side */
local SPAWN_ZOMBIE = 8;
local SpawnZombie =
[
    //x, y, z, angle
    [5587.17,367.20,-5857.20,76], //1
	[6380.95,368.20,-3531.90,120], //2
	[8571.92,368.20,-5834.56,309], //3
	[8461.10,368.25,-3832.24,218], //4
	[6131.65,368.25,-4315.43,81], //5
	[7715.59,368.10,-3396.38,241], //6
	[8049.91,368.20,-6227.58,2], //7
	[7653.45,368.20,-3988.10,139] //8
];

local SPAWN_HUMAN = 5;

local SpawnHuman =
[
    //x, y, z, angle
    [12467.02,998.20,-2261.10,223], //1
	[13221.27,998.20,-3018.44,273], //2
	[11435.20,998.20,-3104.30,76], //3
	[11826.88,998.20,-3812.57,24], //4
	[14046.03,1187.80,-1371.67,226] //5
];

local AWAIT_MINUTE = 0;
local AWAIT_SECOND = 59;
local ROUND_MINUTE = 4;
local ROUND_SECOND = 59;

local Game = {};
Game.started <- false;
Game.zombies <- 0;
Game.humans <- 0;
Game.minute <- AWAIT_MINUTE;
Game.second <- AWAIT_SECOND;

local Player = {};
for(local i = 0; i < getMaxSlots(); ++i)
{
    Player[i] <- {};
    Player[i].team <- -1;
}

function gameTextForPlayer(id, x, y, text, font, r, g, b, time)
{
    packet <- Packet();
    packet.writeUInt8(MessageId.GAME_TEXT);
    packet.writeUInt16(x);
    packet.writeUInt16(y);
    packet.writeUInt8(r);
    packet.writeUInt8(g);
    packet.writeUInt8(b);
    packet.writeUInt16(time);
    packet.writeString(font);
    packet.writeString(text);
    packet.send(id, RELIABLE);
}

function gameTextForAll(x, y, text, font, r, g, b, time)
{
    for(local i = 0; i < getMaxSlots(); ++i)
        if(isPlayerConnected(i) == true)
            gameTextForPlayer(i, x, y, text, font, r, g, b, time);
}

function irand(max)
{
    local roll = (1.0 * rand() / RAND_MAX) * (max + 1);
    return roll.tointeger();
}

function setZombie(pid)
{
    if(Player[pid].team == Teams.HUMAN)
        Game.humans--;
    if(Player[pid].team != Teams.ZOMBIE)
        Game.zombies++;
    Player[pid].team = Teams.ZOMBIE;
    packet <- Packet();
    packet.writeUInt8(MessageId.SET_ZOMBIE);
    packet.send(pid, RELIABLE);
    setPlayerColor(pid, 255, 0, 0);

    local rand = irand(3)+1;
    switch(rand)
    {
         case 1: ZOMBIE01(pid); break;
         case 2: ZOMBIE02(pid); break;
         case 3: ZOMBIE03(pid); break;
         case 4: ZOMBIE04(pid); break;
    }
    local spawn = irand(SPAWN_ZOMBIE-1);
    setPlayerPosition(pid, SpawnZombie[spawn][0], SpawnZombie[spawn][1], SpawnZombie[spawn][2]);
    setPlayerAngle(pid, SpawnZombie[spawn][3]);
}

function setHuman(pid)
{
    if(Player[pid].team == Teams.ZOMBIE)
        Game.zombies--;
    if(Player[pid].team != Teams.HUMAN)
        Game.humans++;
    Player[pid].team = Teams.HUMAN;
    packet <- Packet();
    packet.writeUInt8(MessageId.SET_HUMAN);
    packet.send(pid, RELIABLE);
    setPlayerColor(pid, 0, 255, 0);

    local rand = irand(3)+1;
    switch(rand)
    {
        case 1: PIRATE(pid); break;
        case 2: DRAGONHUNTER(pid); break;
        case 3: KNIGHT(pid); break;
        case 4: ACROBAT(pid); break;
    }
    local spawn = irand(SPAWN_HUMAN-1);
    setPlayerPosition(pid, SpawnHuman[spawn][0], SpawnHuman[spawn][1], SpawnHuman[spawn][2]);
    setPlayerAngle(pid, SpawnHuman[spawn][3]);
}

function setRandomZombie()
{
    if(Game.humans > 0)
    {
        local availablePlayers = [];
        for(local i = 0; i < getMaxSlots(); ++i)
        {
            if(isPlayerConnected(i) == true && Player[i].team == Teams.HUMAN)
            {
                availablePlayers.push(i);
            }
        }
        local r = irand(availablePlayers.len()-1);
        setZombie(availablePlayers[r]);
        return true;
    }
    return false;
}

function beginAwait()
{
    Game.started = false;
    Game.minute = AWAIT_MINUTE;
    Game.second = AWAIT_SECOND;
    //Set all to human
    for(local i = 0; i < getMaxSlots(); ++i)
        if(isPlayerConnected(i) == true)
            setHuman(i);
}

function beginRound()
{
    //Set random zombie
    if(setRandomZombie() == true)
    {
        Game.started = true;
        Game.minute = ROUND_MINUTE;
        Game.second = ROUND_SECOND;
    }
    else beginAwait();
}

function checkHumans()
{
    if(Game.humans == 0)
    {
		gameTextForAll(200,3000,"Humans didn't survive. Zombies again took over human life.","Font_Old_20_White_Hi.TGA",255,0,0,7000);
		beginAwait();
    }
}

function checkZombies()
{
    if(Game.zombies == 0)
    {
        if(setRandomZombie() == false)
        {
            gameTextForAll(1000,3000,"Can't continue game. No active humans and zombies.","Font_Old_20_White_Hi.TGA",255,0,0,5000);
			beginAwait();
        }
    }
}

local gameTimer = setTimer(function()
{
    if(Game.started == true)
    {
        checkHumans();
        checkZombies();
    }
    else
    {
        //Check zombie bug
        for(local i = 0; i < getMaxSlots(); ++i)
        {
            if(isPlayerConnected(i) == true)
            {
                if(Player[i].team == Teams.ZOMBIE)
                    setHuman(i);
            }
        }
    }
    //Update time
    if(Game.second > 0)
        Game.second--;
    else if(Game.second == 0)
    {
        if(Game.minute > 0)
        {
            Game.minute--;
            Game.second = 59;
        }
        else if(Game.minute == 0)  
        {
            if(Game.started == true)
            {
                if(Game.humans > 0) beginAwait(); //Humans win
            }
            else beginRound();
        }
    }
    //Update clients
    packet <- Packet();
    packet.writeUInt8(MessageId.CLIENT_UPDATE);
    packet.writeBool(Game.started);
    packet.writeUInt8(Game.minute);
    packet.writeUInt8(Game.second);
    packet.writeUInt8(Game.zombies);
    packet.writeUInt8(Game.humans);
    for(local i = 0; i < getMaxSlots(); ++i)
        if(isPlayerConnected(i) == true)
            packet.send(i, RELIABLE);
    //Area lock
    for(local i = 0; i < getMaxSlots(); ++i)
    {
        if(isPlayerConnected(i) == true)
        {
            if(Player[i].team == Teams.HUMAN)
            {
                local pos = getPlayerPosition(i);
                if(getDistance3d(pos.x, pos.y, pos.z, 9287.83, 925.16, -4676.88) <= 700)
                {
                    gameTextForPlayer(i, 1500,3000,"You have to defend the fort against zombie!","Font_Old_20_White_Hi.TGA",255,255,0,5000);
                    setPlayerPosition(i, 10979,998,-4047);
                    setPlayerAngle(i, 240);
                }
            }
        }
    }
    
}, 1000, 0);

addEventHandler("onInit", function()
{
    print("----------------------------------------------");
	print("Zombie mode by Risen loaded (G2O port: Sative)");
	print("----------------------------------------------");

    setServerDescription("Wave of zombies 1.3");
});

addEventHandler("onPlayerJoin", function(pid)
{
    sendMessageToAll(0, 255, 0, format("%s (ID:%d) connected to server.", getPlayerName(pid), pid));
    setPlayerColor(pid, 255, 255, 255);
    if(Game.started == true)
    {
        gameTextForPlayer(pid,1500,3000,"Too late! You joined to the Zombie Team.","Font_Old_20_White_Hi.TGA",255,0,0,5000);
        setZombie(pid);
    }
    else
    {
        gameTextForPlayer(pid,250,3000,"Round has not else started. You joined to the Human Team.","Font_Old_20_White_Hi.TGA",255,255,0,5000)
		setHuman(pid);
    }
    spawnPlayer(pid);
});

addEventHandler("onPlayerDisconnect", function(pid, reason)
{
    sendMessageToAll(255, 0, 0, format("%s (ID:%d) disconnected.", getPlayerName(pid), pid));
    if(Player[pid].team == Teams.ZOMBIE)
        Game.zombies--;
    if(Player[pid].team == Teams.HUMAN)
        Game.humans--;
    Player[pid].team = -1;
});

addEventHandler("onPlayerRespawn", function(pid)
{
    setZombie(pid);
    spawnPlayer(pid);
});

addEventHandler("onPlayerMessage", function(pid, message)
{
    print(format("%s: %s", getPlayerName(pid), message));
    sendPlayerMessageToAll(pid, 255, 255, 255, message);
});

function ZOMBIE01(pid)
{
    setPlayerInstance(pid, "ZOMBIE01");
    setPlayerMaxHealth(pid, 5000);
    setPlayerHealth(pid, 5000);
    setPlayerStrength(pid, 200);
}

function ZOMBIE02(pid)
{
    setPlayerInstance(pid, "ZOMBIE02");
    setPlayerMaxHealth(pid, 5000);
    setPlayerHealth(pid, 5000);
    setPlayerStrength(pid, 200);
}

function ZOMBIE03(pid)
{
    setPlayerInstance(pid, "ZOMBIE03");
    setPlayerMaxHealth(pid, 5000);
    setPlayerHealth(pid, 5000);
    setPlayerStrength(pid, 200);
}

function ZOMBIE04(pid)
{
    setPlayerInstance(pid, "ZOMBIE04");
    setPlayerMaxHealth(pid, 5000);
    setPlayerHealth(pid, 5000);
    setPlayerStrength(pid, 200);
}

function PIRATE(pid)
{
    setPlayerInstance(pid, "PC_HERO");
    setPlayerVisual(pid, "Hum_Body_Naked0", 3, "Hum_Head_Psionic", 59);
    setPlayerMaxHealth(pid, 300);
    setPlayerHealth(pid, 300);
    setPlayerStrength(pid, 145);
    setPlayerSkillWeapon(pid, WEAPON_2H, 60);

    giveItem(pid, Items.id("ITAR_PIR_H_ADDON"), 1);
    giveItem(pid, Items.id("ITMW_KRUMMSCHWERT"), 1);

    equipItem(pid, Items.id("ITAR_PIR_H_ADDON"));
    equipItem(pid, Items.id("ITMW_KRUMMSCHWERT"));
}

function DRAGONHUNTER(pid)
{
    setPlayerInstance(pid, "PC_HERO");
    setPlayerVisual(pid, "Hum_Body_Naked0", 2, "Hum_Head_Fighter", 120);
    setPlayerMaxHealth(pid, 300);
    setPlayerHealth(pid, 300);
    setPlayerStrength(pid, 90);
    setPlayerDexterity(pid, 50);
    setPlayerSkillWeapon(pid, WEAPON_1H, 60);
    setPlayerSkillWeapon(pid, WEAPON_CBOW, 50);

    giveItem(pid, Items.id("ITRW_BOLT"), 100);
    giveItem(pid, Items.id("ITAR_DJG_M"), 1);
    giveItem(pid, Items.id("ITMW_RUBINKLINGE"), 1);
    giveItem(pid, Items.id("ITRW_CROSSBOW_L_02"), 1);

    equipItem(pid, Items.id("ITAR_DJG_M"));
    equipItem(pid, Items.id("ITMW_RUBINKLINGE"));
    equipItem(pid, Items.id("ITRW_CROSSBOW_L_02"));
}

function KNIGHT(pid)
{
    setPlayerInstance(pid, "PC_HERO");
    setPlayerVisual(pid, "Hum_Body_Naked0", 1, "Hum_Head_FatBald", 32);
    setPlayerMaxHealth(pid, 300);
    setPlayerHealth(pid, 300);
    setPlayerStrength(pid, 120);
    setPlayerDexterity(pid, 80);
    setPlayerSkillWeapon(pid, WEAPON_2H, 60);
    setPlayerSkillWeapon(pid, WEAPON_BOW, 50);

    giveItem(pid, Items.id("ITRW_ARROW"), 100);
    giveItem(pid, Items.id("ITAR_PAL_M"), 1);
    giveItem(pid, Items.id("ITMW_2H_PAL_SWORD"), 1);
    giveItem(pid, Items.id("ITRW_BOW_M_02"), 1);

    equipItem(pid, Items.id("ITAR_PAL_M"));
    equipItem(pid, Items.id("ITMW_2H_PAL_SWORD"));
    equipItem(pid, Items.id("ITRW_BOW_M_02"));
}

function ACROBAT(pid)
{
    setPlayerInstance(pid, "PC_HERO");
    setPlayerVisual(pid, "Hum_Body_Naked0", 2, "Hum_Head_Thief", 95);
    setPlayerMaxHealth(pid, 300);
    setPlayerHealth(pid, 300);
    setPlayerStrength(pid, 50);
    setPlayerDexterity(pid, 100);
    setPlayerSkillWeapon(pid, WEAPON_1H, 40);
    setPlayerSkillWeapon(pid, WEAPON_BOW, 90);

    giveItem(pid, Items.id("ITRW_ARROW"), 300);
    giveItem(pid, Items.id("ITAR_DIEGO"), 1);
    giveItem(pid, Items.id("ITMW_FRANCISDAGGER_MIS"), 1);
    giveItem(pid, Items.id("ITRW_BOW_M_03"), 1);

    equipItem(pid, Items.id("ITAR_DIEGO"));
    equipItem(pid, Items.id("ITMW_FRANCISDAGGER_MIS"));
    equipItem(pid, Items.id("ITRW_BOW_M_03"));
}