# Introduction

This gamemode is a **port of server "Zombie Mode" by Risen** from Gothic Multiplayer platform by **Sative**

## Gameplay

* Gameplay video recorded by **Kejn** - https://youtu.be/7jSB1dWlX18

# Installation

* Include `scripts.xml` to your project, this can be done by adding this line to your `config.xml` file.  

Example:

```xml
<import src="gamemodes/zombiemode/scripts.xml" />
```